import sys
import os
import json
import re
from pprint import pprint
import logging

from lib.grub_shops.dveribelorussii import Dveri
from lib.grub_shops.tovar import TovarEncoder
from lib.grub_shops.tovar import Tovar
from lib.grub_shops.platforms.prom_ua import Prom

from selenium.common.exceptions import WebDriverException
from requests.exceptions import ConnectionError

levels = {
    'CRITICAL': logging.CRITICAL,
    'ERROR': logging.ERROR,
    'WARNING': logging.WARNING,
    'INFO': logging.INFO,
    'DEBUG': logging.DEBUG
}

try:
    logging.basicConfig(level=levels[sys.argv[-1]])
except (IndexError, KeyError):
    logging.basicConfig(level=logging.WARNING)


def get_all_specs(data):
    specs = set()
    for k, prod in data.items():
        for spec in prod.specs:
            specs.add(spec)
    pprint(specs)


def process(data):
    pass


TASK = sys.argv[1]
TMP_FILE = './data/grub.dat'
GPRODS_FILE = './data/grubbed.json'
XLSX_FILE = './data/dveri.xlsx'
DEBUG = sys.argv[-1].lower() == 'debug'
EXCEPTIONS = (KeyboardInterrupt) if DEBUG else (KeyboardInterrupt,
                                                AttributeError)


if TASK == 'grub':
    grubber = Dveri()
    if os.path.exists(TMP_FILE):
        grubber.load(TMP_FILE)

    try:
        grubber.grub_all()
        grubber.process_filters()
        grubber.grub_details()
        grubber.dump(TMP_FILE)
    except EXCEPTIONS:
        grubber.dump(TMP_FILE)
        raise

    json.dump(grubber.get_gprods(), open(GPRODS_FILE, 'w'), cls=TovarEncoder)

elif TASK == 'export':
    data = json.load(open(GPRODS_FILE))
    data = {k: Tovar.from_dict(v) for k, v in data.items()}
    data = process(data)  # Если нужно
    get_all_specs(data)
    prom = Prom()
    prom.gprods_to_xls(list(data.values()), XLSX_FILE)
    # prom.dump_categories('./data/prom_categories.dat')

elif TASK == 'process':
    data = json.load(open(GPRODS_FILE))
    data = {k: Tovar.from_dict(v) for k, v in data.items()}
    data = process(data)
    json.dump(data, open(GPRODS_FILE, 'w'), cls=TovarEncoder)
