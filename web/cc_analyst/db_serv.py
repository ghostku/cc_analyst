from cc_analyst import db
from cc_analyst.models import Call


def remove_from_db(_from, till):
    res = db.session.query(Call).filter(Call.result_date.between(_from, till))
    cnt = res.count()
    for call in res:
        db.session.delete(call)
    db.session.commit()
    # TODO: Вернуть количество реально удаленных или ошибку
    return cnt
