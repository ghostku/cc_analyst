# My General Functions
# 2015-11-29
# 2015-12-01
# 2015-12-19 Add getImage() & cutBySpaces()

# 12.05.2016 Add iru2uri to GetHTML function now it can work with non
# ascii URLs

# Возвращает введенную с клавиатуры строку или default если с клавиатуры ничего не ввели

# 12.06.2016 [ADDED] Debug mode for get_HTML_page
# 12.06.2016 [MODIFIED] PEP8
import json as json
import datetime
import sys


def upload_to_FTP(ftp, file):
    from ftplib import FTP
    session = FTP(ftp['SERVER'], ftp['LOG'], ftp['PWD'])
    file = open(file, 'rb')                              # file to send
    session.storbinary('STOR ' + ftp['PATH'], file)      # send the file
    file.close()                                         # close file and FTP
    session.quit()


def print_one_line(text, separator='...'):
    print(text, end=separator)
    sys.stdout.flush()


def input_def(text, default):
    res = input(text + ' [{default}]: '.format(default=default))
    res = res or default
    return res


def get_random_string(N):  # Generate a random string of ASCII chars size of N
    import random
    import string
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))


def chooseOneOf(text, variant, default=''):
    variants = list(enumerate(variant, 1))
    check = [str(i[0]) for i in variants]
    text += ' [' + \
        ', '.join(['{num} - {el}'.format(num=i[0], el=i[1])
                   for i in variants]) + ']'
    res = input_def(text, default)
    while (res not in check) and (res != default):
        res = input_def(text, default)
    if res == default:
        return res
    else:
        return variants[int(res) - 1][1]


def saveJSONToFile(filename, stream, codepage='utf-8', encoder=None):
    resf = open(filename, encoding=codepage, mode='w')
    json.dump(stream, resf, cls=encoder)
    resf.close()



def loadJSONFromFile(filename):
    with open(filename, encoding="utf-8") as f:
        return json.load(f, encoding="utf-8")


def savePage2File(url, file):
    f = open(file, encoding='utf-8', mode='w')
    src = urllib.request.urlopen(url)
    encod = src.headers.get_content_charset()
    if encod is None:
        encod = "cp1251"
    src = src.read().decode(encod)
    f.write(src)
    f.close()


def cleanWinFilename(filename):
    import re
    return re.sub('[^\w\-_\. ]', '_', filename)


def get_HTML_page(url, debug=False):  # Get HTML page as lxml object
    import urllib.request
    from urllib.error import HTTPError
    # from urllib.parse import urlsplit, urlunsplit, quote
    import lxml.html as html
    from httplib2 import iri2uri
    url = iri2uri(url)
    try:
        src = urllib.request.urlopen(url)
    except HTTPError as err:
        print("HTTP error: {0} url:{1}".format(err, url))
        return err
    encod = src.headers.get_content_charset()
    if encod is None:
        encod = "cp1251"
    src = src.read().decode(encod, 'ignore')
    if debug:
        f = open('.\\debug.html', encoding='utf-8', mode='w')
        f.write(src)
        f.close()
    return html.document_fromstring(src)


def encodeURL(url):  # UTF-8 URL Escaping
    # import urllib
    import urllib.parse as urlparse
    parts = urlparse.urlsplit(url)
    parts = parts._replace(path=urlparse.quote(parts.path.encode('utf8')))
    return parts.geturl()


def getImage(url, filename):  # Download image with requests
    import requests
    import shutil
    import os
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    r = requests.get(url, stream=True)
    if r.status_code == 200:
        with open(filename, 'wb') as f:
            r.raw.decode_content = True
            shutil.copyfileobj(r.raw, f)


# Обрезает строку до максимального количества слов не превышающих заданную
# длину
def cutBySpaces(src, size):
    src = src.split(' ')
    res = ''
    for s in src:
        res1 = res + ' ' + s
        if len(res1) - 1 > size:
            return res.strip(' ')
        res = res1
    return res.strip(' ')


def url2dict(url):  # TODO Move to gen_fun
    """Returns a dictionnary that contains parsed URL data divided by:
        `?`, `&` and `=`

    Args:
        url (string): URL to parse

    Returns:
        dict: result {'base':base,'params':{'param_1':val_1,'param_2':val_2,...}}

    Examples:
        >>> url2dict("http://any.com/path/file.html?param_1=val_1&param_2=val_2") \
        == {'param': {'param_2': 'val_2', 'param_1': 'val_1'}, 'base': \
        'http://any.com/path/file.html'}
        True
    """
    base, param = url.split('?', 2)
    param = {i.split('=')[0]: i.split('=')[1] for i in param.split('&')}
    return {'base': base, 'param': param}


def dict2url(d):  # TODO Clean It
    return '?'.join([d['base'], '&'.join(['='.join([i, d['param'][i]]) for i in d['param']])])


class Profiler(object):
    def __enter__(self):
        self._start_time = datetime.datetime.now()

    def __exit__(self, type, value, traceback):
        self._finish_time = datetime.datetime.now()
        # print ("Elapsed time: {:.3f} sec".format(time.time() - self._startTime))
        print("{name}\t\aStarted at {stime}, Ended at {ftime}, So it tooks {delta}".format(
            name=self._name, stime=self._start_time, ftime=self._finish_time, delta=self._finish_time - self._start_time))

    def __init__(self, name=''):
        self._name = str(name)

    def get_time(self):
        return datetime.datetime.now() - self._start_time

import time, datetime


class Timer(object):
    def __init__(self, verbose=False):
        self.verbose = verbose

    def __enter__(self):
        self.start = time.time()
        return self

    def __exit__(self, *args):
        self.end = time.time()
        self.secs = self.end - self.start
        self.msecs = self.secs * 1000
        self.hms = str(datetime.timedelta(seconds=round(self.secs)))
        if self.verbose:
            print('Время выполения: %f ms' % self.msecs)


def unique_list(seq, idfun=None):  # Alex Martelli ******* order preserving make list unique
    if idfun is None:
        def idfun(x):
            return x
    seen = {}
    result = []
    for item in seq:
        marker = idfun(item)
        # in old Python versions:
        # if seen.has_key(marker)
        # but in new ones:
        if marker not in seen:
            seen[marker] = 1
            result.append(item)
    return result
