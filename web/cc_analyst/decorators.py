"""
Немного специальных декораторов
"""
from threading import Thread


def async(f):
    """ Декоратор для асинхронного запуска функций

    Args:
        f (TYPE): Description

    Returns:
        TYPE: Description
    """
    def wrapper(*args, **kwargs):
        thr = Thread(target=f, args=args, kwargs=kwargs)
        thr.start()
    return wrapper
