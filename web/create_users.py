#!/usr/bin/python3
from cc_analyst import db, models
from flask_security import SQLAlchemyUserDatastore

# Setup Flask-Security
user_datastore = SQLAlchemyUserDatastore(db, models.User, models.Role)

# Create a user to test with
user_datastore.create_user(email='bringo', password='bringo', allowed_areas_id='3')
user_datastore.create_user(email='sumy', password='sumy', allowed_areas_id='2')
user_datastore.create_user(email='zenko', password='zenko', allowed_areas_id='')
user_datastore.create_user(email='kyiv', password='kyiv', allowed_areas_id='1')
user_datastore.create_role(name='manage')
user_datastore.create_role(name='upload')
user_datastore.create_role(name='view')
user_datastore.add_role_to_user(user='zenko', role='manage')
user_datastore.add_role_to_user(user='zenko', role='upload')



db.session.commit()
