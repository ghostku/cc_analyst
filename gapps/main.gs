function onOpen() {
  SpreadsheetApp.getUi() // Or DocumentApp or FormApp.
  .createMenu('CC Analyst')
  .addItem('Отчёт по оператору(ам)', 'byOperatorDialog')
  .addItem('Отчёт по периоду', 'byPeriodDialog')
  .addItem('Отчёт по параметру', 'byParamDialog')
  .addItem('Специальный отчёт', 'specialDialog')
  .addItem('Настройки', 'settingsDialog')
  .addToUi(); 
}

function saveSettings(data) {
  var properties = PropertiesService.getScriptProperties();
  properties.setProperty('LOG', data['LOG']);
  properties.setProperty('PWD', data.PWD);
  properties.setProperty('URL', data.URL);
  properties.setProperty('OPERATORS_URL', data.URL + 'get_operators');
  properties.setProperty('AREAS_URL', data.URL + 'get_areas');
  properties.setProperty('PARAMS_URL', data.URL + 'get_params');
  properties.setProperty('REPORT_URL', data.URL + 'get_report');
  return 0;
}

function settingsDialog(){
  var template = HtmlService.createTemplateFromFile('settings')
  var properties = PropertiesService.getScriptProperties();
  template.LOG = properties.getProperty('LOG');
  template.PWD = properties.getProperty('PWD');
  template.URL = properties.getProperty('URL');
  
  var html = template.evaluate()
      .setTitle('Настройки')
      .setWidth(600); 
  SpreadsheetApp.getUi().showSidebar(html);
}

function byOperatorDialog() { showDialog('by_operator', 'Отчёт по оператору(ам)'); }
function byPeriodDialog() { showDialog('by_period', 'Отчёт по периоду'); }
function byParamDialog() { showDialog('by_param', 'Отчёт по параметру'); }
function specialDialog() { showDialog('special', 'Специальный отчёт', ['#to','#p-operators', '#p-params', '#p-period-type']); }
 
Date.prototype.getFirstDayInMonth = (function() {
  var local = new Date(this);
  local.setMinutes(this.getMinutes() - this.getTimezoneOffset())
  local.setDate(1)
  return local.toJSON().slice(0,10);
});
 
Date.prototype.getLastDayInMonth = (function() {
  var local = new Date(this);
  local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
  local.setMonth(local.getMonth() + 1);
  local.setDate(0)
  return local.toJSON().slice(0,10);
});
  
function showDialog(task_type, title, inputs) {
  var template = HtmlService.createTemplateFromFile('ui')
  template.task_type = task_type
  template.inputs = inputs
  var html = template.evaluate()
      .setTitle(title)
      .setWidth(600); 
  SpreadsheetApp.getUi() // Or DocumentApp or FormApp.
      .showSidebar(html);
}
/**
* Generates full URL by adding parameters to base URL
* url - base URL like 'http://site.com/page'
* params - dict of parameters like {'name_1':'value_1','name_2':'value_2'}
* return - http://site.com/page?name_1=value_1&name_2=value_2
**/
function generateURL(url, params){
  param_list = [];
  for (param in params){
    param_list.push(param + '=' + params[param])
  }
  return [url, param_list.join('&')].join('?');
}

function generateHeaders(){
    var options = {
    'headers': {
      'Authorization': 'Basic ' + Utilities.base64Encode(
      PropertiesService.getScriptProperties().getProperty('LOG') 
      + ':' + 
      PropertiesService.getScriptProperties().getProperty('PWD'))
    }
  };
  return options
}
function createLabel(cell, cols, header, text, font_size, format){
  range_1 = cell.offset(0, 0, 1, cols);
  range_2 = cell.offset(1, 0, 3, cols);
  range_1.merge().setValue(header).setFontSize(10).setFontStyle('normal').setFontWeight('bold').setHorizontalAlignment('center').setBorder(true, true, true, true, null, null).setBackground('white');
  range_2.merge().setValue(text).setFontSize(font_size).setFontStyle('italic').setHorizontalAlignment('center').setVerticalAlignment('middle').setBorder(true, true, true, true, null, null).setNumberFormat(format).setBackground('white');
}

/**
* Request list of operators from server
*
*/
function getOperators(){
  var data = JSON.parse(UrlFetchApp.fetch(PropertiesService.getScriptProperties().getProperty('OPERATORS_URL'), generateHeaders()).getContentText());
  res = []
  for (i=0;i<data.length;i++){
    res.push([data[i].login, '(' + data[i].id_ + ') ' + data[i].login]);
  }
  return res
}

/**
* Request list of available parameters from server
* And report them as dictonnary to create list of select options
**/
function getParams(){
  res = []
  for (i=0; i<DEFAULTS.length; i++){
    res.push([DEFAULTS[i]['id'], DEFAULTS[i]['header']]);
  }
  return res;
}


/**
* Request a list of used areas from server
* And report them as dictonnary to create list of select options
**/
function getAreas() {
  var data = JSON.parse(UrlFetchApp.fetch(PropertiesService.getScriptProperties().getProperty('AREAS_URL'), generateHeaders()).getContentText());
  res = []
  for (i=0;i<data.length;i++){
    res.push([data[i].id, data[i].name]);
  }
  return res;
}
function getJSON(url){
//  var url = generateURL(baseURL, params);
  
  var data = JSON.parse(UrlFetchApp.fetch(url, generateHeaders()).getContentText());
  while (data['status'] == 'WAIT'){
    Utilities.sleep(10000);
    data = JSON.parse(UrlFetchApp.fetch(url + '&report_id=' + data.report_id, generateHeaders()).getContentText());
  }
  return data
}

/**
* Prepare configuration data
* convert from DEFAULTS array to three dicts: Names, Formats, Colors
**/
function prepareDefaults(src) {
  res = {'id2Format': {}, 'id2Name': {}, 'id2Color': {}, 'name2Format': {}, 'name2Color': {}}
  for (i in src) {
    res['id2Format'][src[i]['id']] = src[i]['format'];
    res['id2Name'][src[i]['id']] = (src[i]['header']);
    res['id2Color'][src[i]['id']] = (src[i]['color']);
    res['name2Format'][src[i]['header']] = (src[i]['format']);
    res['name2Color'][src[i]['header']] = (src[i]['color']);
  }
  return res
}

function clearSheet(sheet){
  sheet.clear();
  sheet.getRange(1, 1, sheet.getMaxRows(), sheet.getMaxColumns()).setBackground('#F3F3F3');
  sheet.setFrozenColumns(0)
  sheet.setFrozenRows(0)
}

function getReport(request){
  var type = request['task_type']
  if (type == 'by_operator') {getReportCommon(request)}
  if (type == 'by_period') {getReportCommon(request)}
  if (type == 'by_param') {getReportCommon(request)}
  if (type == 'special') {getReportSpecial(request)}
}

function getReportSpecial(request){
  
  function drawFirstTable(cell, request){
    
    request.period = 'month';
    request.params = 'calls_succes_ok,calls_succes_reject,prk_ok,pairs,ok_per_pair',
    request.task_type = 'by_period';
    request.rotate = 0;
    url = generateURL(PropertiesService.getScriptProperties().getProperty('REPORT_URL') , request);

    cell.setValue('Loading. Please wait....');
  SpreadsheetApp.flush();
 
  data = getJSON(url);
  cell.setValue(null);
  colHNAMES = {
    'calls_succes_ok': 'Заявки',
    'calls_succes_reject': 'Отказы',
    'prk_ok': 'ПрК',
    'pairs': 'Смены',
    'ok_per_pair': 'Index З/С',
  }
  drawTable(data['response'], [], [], cell, true, conf['id2Format'], conf['id2Color'], {}, colHNAMES);
}
  
  /** Second Table **/
  function drawSecondTable(cell, request) {

    request.period = 'day';
    request.params = 'calls_succes_ok,calls_succes_reject,prk_ok';
    request.task_type = 'by_param';
    request.rotate = 0;
  
    url = generateURL(PropertiesService.getScriptProperties().getProperty('REPORT_URL') , request);
    cell.setValue('Loading. Please wait....');
    SpreadsheetApp.flush();
    data = getJSON(url);
    cell.setValue(null);
    colHNAMES = {
      'calls_succes_ok': '  (z)  ',
      'calls_succes_reject': '  О  ',
      'prk_ok': 'ПрК',
      'pairs': 'Смены',
      'ok_per_pair': 'Index З/С',
    }
    drawTable(data['response'], [], [], cell, true, conf['id2Format'], conf['id2Color'], {}, colHNAMES);
  }
  
  var conf = prepareDefaults(DEFAULTS);
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();
  clearSheet(sheet);
  request.from = getDateFromYMD(request.from);
  request.to = request.from.getLastDayInMonth()
  request.from = request.from.getFirstDayInMonth();
  drawSecondTable(sheet.getRange('C1'), request);
  drawFirstTable(sheet.getRange('A2'), request);
  sheet.setFrozenColumns(6)
  sheet.setFrozenRows(2)
}



function getReportCommon(request){
  var conf = prepareDefaults(DEFAULTS);
  var type = request['task_type']
  request['rotate']  = (type == 'by_operator') ? 1: 0;
  
  // Form URL
  url = generateURL(PropertiesService.getScriptProperties().getProperty('REPORT_URL'), request);
//  Logger.log(url);
//  var app = SpreadsheetApp;
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();
  var cell = sheet.getRange("B12");
  
  
  clearSheet(sheet);
  cell.setValue('Loading. Please wait....');
  SpreadsheetApp.flush();
  
  data = getJSON(url);
  var type = data['request']['task_type']
  cell.setValue(null);
  
  if (type == 'by_operator'){
    data1 = {}
    rowsOrder = []
    for (i=0;i<BY_OPERATORS_ROWS.length;i++){
      if (BY_OPERATORS_ROWS[i]['field'] in data['response']) {
        rowsOrder.push(BY_OPERATORS_ROWS[i]['header'])
        data1[BY_OPERATORS_ROWS[i]['header']] = data['response'][BY_OPERATORS_ROWS[i]['field']]
      }             
    }
    data['response'] = data1
  }
  else {rowsOrder = []}
  var FORMATS = (type == 'by_operator') ? conf['name2Format'] : conf['id2Format']
  var COLORS = (type == 'by_operator') ? conf['name2Color'] : conf['id2Color']
  var NAMES = (type == 'by_operator') ? [] : conf['id2Name']
  var formatByColumn = (type == 'by_operator') ?  false : true
  sheet.getRange(1, 1, sheet.getMaxRows(), sheet.getMaxColumns()).setBackground('#F3F3F3');
  drawTable(data['response'], rowsOrder, [], cell, formatByColumn, FORMATS, COLORS, NAMES, NAMES);
  sheet.setFrozenColumns(3)
  if (type == 'by_operator') {
    var date = data['request']['from'].split(' ')[0] + '\n' + data['request']['to'].split(' ')[0]
    var pairs = data['response']['Смен']['ВСЕГО'];
    var calls_ok_succes = data['response']['Заявок']['ВСЕГО'];
    var prk_ok = data['response']['KPI ПрК']['ВСЕГО'];
    var ok_per_pair = data['response']['KPI Index З/С']['ВСЕГО'];
    var operators = data['request']['operators'];
    
    createLabel(sheet.getRange('B2'), 1, 'Название локации', 'Zenko', 24, '');
    createLabel(sheet.getRange('B7'), 1, 'Период', date, 24, '');
    createLabel(sheet.getRange('K2'), 2, 'Смен операторов', pairs, 36, '0');
    createLabel(sheet.getRange('H2'), 2, 'KPI ПрК', prk_ok, 36, '0.0%');
    createLabel(sheet.getRange('E7'), 11, 'Оператор(ы)', operators , 24, '');
    createLabel(sheet.getRange('E2'), 2, 'INDEX З/С', ok_per_pair, 36, '0.00');
    createLabel(sheet.getRange('N2'), 2, 'Заявок оформлено', calls_ok_succes, 36, '0');
  }
  SpreadsheetApp.flush();
}

/**
 * Show table on sheet from JSON data
 * @param  {dict} data               data to show in at least 2-dimensional dict
 * @param  {list} rowsOrder          ordered list of rows
 * @param  {list} colsOrder          ordered list of columns
 * @param  {Range} cell              starting cell for table placement
 * @param  {bool} formatByCol        what header should we use to find format and collor of cell: colHeader or rowHeader
 * @param  {dict} FORMATS            NumericFormats for cells
 * @param  {dict} COLORS             Background colors for cells
 * @param  {dict} rowHNAMES          Dict to rename rowHeaders
 * @param  {dict} colHNAMES          Dict to rename colHeaders
 * @param  {int} frozenCols          How many cols should we freeze on page
 * @param  {int} frozenRows          How many rows should we freeze on page
 */

function drawTable(data, rowsOrder, colsOrder, cell, formatByCol, FORMATS, COLORS, rowHNAMES, colHNAMES){
  var sheet = cell.getSheet();  
  
  
  var rowHeaders = 1;
  var head = data[Object.keys(data)[0]];
  var headers = [];
  while (typeof(head) == 'object') {
  header = [];
  for (i in head) { 
    header.push(i); 
  }
  headers.push(header);
  head = head[Object.keys(head)[0]];
  }
  
//  // Remove hear if it consists of only one element
//  for (header in headers){
//    if (headers[header].length == 1) { headers.splice(header, 1) }
//  }
  // * Calculate header's width * How many column will take one header on each level
  var headerLength = []
  headerLength[headers.length - 1] = 1
  for (i=headers.length - 2; i>= 0; i--){ 
    headerLength[i] = headers[i + 1].length * headerLength[i+1];
  }
  
  var headerRepeat = []
  headerRepeat[0] = 1
  for (i=1;i<headers.length;i++){
    headerRepeat[i] = headers[i-1].length * headerRepeat[i-1];
  }

  var headersTree = []
  headersTree[headers.length - 1] = []
  for (i in headers[headers.length - 1]){
    headersTree[headers.length - 1].push([headers[headers.length - 1][i]])
  }
  
  for (h= headers.length - 2;h>=0;h--) {
    headersTree[h] = []
    for (i in headers[h]) {
      for (j in headersTree[h + 1]){
        headersTree[h].push([headers[h][i]].concat(headersTree[h + 1][j]))
      }         
    }
  }
  headersTree = headersTree[0];
//  Logger.log(FORMATS);
  
  // Fill rows  
  rows = []; formats = []; colors = []; rowHeaders = []; rowHeaderColors = []
  for (i in data) {
    row = [];  rowFormats = []; rowColors = []
    for (head in headersTree){
      item = data[i]
      for (j in headersTree[head]){
        item = item[headersTree[head][j]]
      }
      if (formatByCol) {
        rowFormats.push(FORMATS[headersTree[head][j]])
        rowColors.push(COLORS[headersTree[head][j]])
      } 
      else {
        rowFormats.push(FORMATS[i])
        rowColors.push(COLORS[i])
      }
      row.push(item);
      
    }
    rows.push(row);
    formats.push(rowFormats);
    colors.push(rowColors);
    rowHeaders.push([(i in rowHNAMES) ? rowHNAMES[i]: i]);
    rowHeaderColors.push([COLORS[i]]);
  }
    range = cell.offset(headers.length, 1, rows.length, rows[0].length)
    range.setValues(rows);
//    Logger.log(formats);
    range.setNumberFormats(formats);
    range.setBackgrounds(colors);
//  Logger.log(rowHeaderColors);
  
  //Fill Rows Headers
  rowHeadRange = cell.offset(headers.length,0,rowHeaders.length,1);
  rowHeadRange.setValues(rowHeaders);
  rowHeadRange.setBackgrounds(rowHeaderColors);
  
  // Fill column headers
  for (h= headers.length - 1;h>=0;h--) {
    var headCell = cell.offset(h,1,1,headerLength[h]);
    for (i=0;i<headerRepeat[h];i++){
      for (j in headers[h]){
        headCell.setValue((headers[h][j] in colHNAMES) ? colHNAMES[headers[h][j]] : headers[h][j])
        headCell.merge().setHorizontalAlignment('center')
        headCell = headCell.offset(0, headerLength[h])
      }  
    }
    if (h == headers.length - 1) {
      for (i= -1;i<range.getWidth();i++){ cell.getSheet().autoResizeColumn(i + range.getColumn()); }  
    }
  }   
  
  
  var lastRow =  sheet.getLastRow() + 1;
  if (sheet.getMaxRows() > lastRow) { sheet.deleteRows(lastRow + 1, sheet.getMaxRows() - lastRow)  }
  var lastCol = Math.max(sheet.getLastColumn() + 1, 16);
  if (sheet.getMaxColumns() > lastCol) { sheet.deleteColumns(lastCol + 1, sheet.getMaxColumns() - lastCol)  }
}

/**
* Just return date object created from string YYYY-MM-DD
**/
function getDateFromYMD(str){
  str = str.split('-');
  str[1] -= 1
  return new Date(str[0], str[1], str[2])
}

function test() {
  form = {'from': '2017-01-01',
          'to': '2017-1-31',
//          'operators': '',
          'period': 'day',
          'params': '',
          'task_type': 'by_operator',
          }
  getReport(form);
}