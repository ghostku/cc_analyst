function show_areas(areas){
	for (i in areas){
		var tr = $('<tr></tr>')
		tr.append($('<td></td>').text(areas[i].name).attr('col', 'name'));
		tr.append($('<td></td>').text(areas[i].id_mark).attr('col', 'id_mark'));
		tr.append($('<button type="button" class="btn btn-danger btn-xs rm-row"><span class="glyphicon glyphicon-trash"></span></button>').attr('id', areas[i].id))//.text('Удалить'))
		$('#areas').append(tr.attr('area_id', areas[i].id).attr('modify', areas[i].modify))
	}

	$(".rm-row").on('click', function(event){
		event.stopPropagation();
		event.stopImmediatePropagation();
		$(this).closest('tr')
			.hide()
			.attr('modify', 'remove')
    });
	$('#areas').editableTableWidget();
    $('#areas td').on('change', function(evt, newValue){
    	$(this).closest('tr').attr('modify', 'modify');
    });
}

function add_area(){
	show_areas([{'name':'Заполните название','id_mark':0,'id':0,'modify':'new'}]);
}

function save_areas(){
	var result = []
	$('#areas > tbody > tr').each(function() {
		var record = {
			'area_id': $(this).attr('area_id'),
			'modify': $(this).attr('modify')
		}
		$(this).find('td').each(function() {
			record[$(this).attr('col')] = $(this).text()
		})
		result.push(record);
	})
	$.ajax({
		type: 'POST',
		contentType: "application/json; charset=utf-8",
		url: $SCRIPT_ROOT + '_areas',
		dataType : 'json',
		data: JSON.stringify(result),
		success: function(data) {
			$('#areas tbody').empty()
			show_areas(data);
		},
		error: function() {
			alert('Some sheet happens')
		}
	});
}