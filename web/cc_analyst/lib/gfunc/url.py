from urllib.parse import urlparse, parse_qs, urlunparse, urlencode

def set_query_parameter(url, param_name, param_value):
    """Given a URL, set or replace a query parameter and return the
    modified URL.

    >>> set_query_parameter('http://example.com?foo=bar&biz=baz', 'foo', 'stuff')
    'http://example.com?foo=stuff&biz=baz'

    """
    scheme, netloc, path, params, query_string, fragment = urlparse(url)
    query_params = parse_qs(query_string)

    query_params[param_name] = [param_value]
    new_query_string = urlencode(query_params, doseq=True)

    return urlunparse((scheme, netloc, path, params, new_query_string, fragment))


def clean_query(url):
    scheme, netloc, path, params, query_string, fragment = urlparse(url)
    return urlunparse((scheme, netloc, path, params, '', fragment))


def add_base(url, base):
    if url.startswith(base):
        return url
    else:
        return base + url


def modify_url(url, new_url):
    import logging
    scheme, netloc, path, params, query, fragment = urlparse(url)
    path = new_url.get('path', path)
    params = urlencode(new_url.get('params', {})) or params
    query = urlencode(new_url.get('query', {})) or query
    fragment = new_url.get('fragment', fragment)
    return urlunparse((scheme, netloc, path, params, query, fragment))
