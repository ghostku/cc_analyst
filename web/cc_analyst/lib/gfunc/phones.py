import logging
import re

PREFIXES = ['\+38', '38', '8', '']
OPERATORS = {
    '050': {'name': 'Vodafone', 'len': 7},
    '095': {'name': 'Vodafone', 'len': 7},
    '099': {'name': 'Vodafone', 'len': 7},
    '066': {'name': 'Vodafone', 'len': 7},

    '067': {'name': 'KyivStar', 'len': 7},
    '097': {'name': 'KyivStar', 'len': 7},
    '096': {'name': 'KyivStar', 'len': 7},
    '098': {'name': 'KyivStar', 'len': 7},
    '068': {'name': 'KyivStar', 'len': 7},

    '063': {'name': 'Life', 'len': 7},
    '073': {'name': 'Life', 'len': 7},
    '093': {'name': 'Life', 'len': 7},

    '044': {'name': 'Киев', 'len': 7}
}


class Phone(object):
    """docstring for Phone"""
    number = None
    def __init__(self, phone):
        super(Phone, self).__init__()
        self.src = phone

    def phone_number_ua(self):
        tmp = self.src
        tmp = re.sub('[^\d]', '', tmp)
        prefixes = '|'.join(PREFIXES)
        operators = '|'.join(OPERATORS)
        tmp = re.search('^(%s)(%s)(.*)' % (prefixes, operators), tmp)
        try:
            self.prefix = tmp.group(1)
            self.operator = tmp.group(2)
            self.number = tmp.group(3)[:OPERATORS[self.operator]['len']]
            self.ext = tmp.group(3)[OPERATORS[self.operator]['len']:]
        except AttributeError:
            logging.error('Cant convert %s number' % self.src)
            return self
        return self

    def get(self, style):
        if not self.number:
            return ''
        return style.format(
            pre=self.prefix, num=self.number, oper=self.operator, ext=self.ext)

    def __str__(self):
        return 'Префикс: %s Оператор: %s Номер: %s Доп.: %s' % (
            self.prefix, self.operator, self.number, self.ext)
