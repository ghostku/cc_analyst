#progressbar.py
# 2015-12-05
import sys
class progressbar_simple(object):
	"""docstring for progressbar_simple"""
	def __init__(self, maxx, minn=1):
		super(progressbar_simple, self).__init__()
		self.maxx = maxx
		self.minn = minn
	def start(self):
		self.show("=>{maxx}<=".format(maxx=self.maxx),": ")
	def stop(self):
		self.show("Done.","\n")
	def show(self,text,end="..."):
		print(text,end=end)
		sys.stdout.flush()
	def get_count(self):
		return self.minn
	def count(self):
		if self.minn>self.maxx:
			print("ProgressBar Error")
			exit(1)
		self.show(self.minn)
		self.minn+=1
class progressbar(object):
	"""docstring for progressbar"""
	def __init__(self, maxx,style="simple",minn=1):
		super(progressbar, self).__init__()
		self.maxx = maxx
		self.style = style
		self.bar = progressbar_simple(maxx,minn)
	def start(self):
		self.bar.start()
	def stop(self):
		self.bar.stop()
	def count(self):
		self.bar.count()
	def get_count(self):
		return self.bar.get_count()
	@property
	def minn(self): return self.bar.minn
if __name__ == "__main__":
	print("Test")
	pb=progressbar(100)
	pb.start()
	for i in range(0,100):
		pb.count()
	pb.stop()
	print("Done")