import os
import csv
import json
import logging
import time
from random import randint
from datetime import datetime
from flask import render_template, request, redirect, flash, Response, url_for
from cc_analyst import app, db, main, storage, data, files, util_converse, db_serv
from cc_analyst.models import Operator, Area, Call, User, JSONEncoder
from cc_analyst.models import get_operators
from werkzeug.utils import secure_filename
import pickle
from flask_security import http_auth_required, roles_accepted, current_user as cu
# from cc_analyst.lib.gfunc.gen_fun import Profiler

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
UPLOAD_FOLDER = os.path.join(APP_ROOT, 'files/')


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/upload', methods=['GET', 'POST'])
@http_auth_required
@roles_accepted('upload')
def upload_file():
    if not os.path.isdir(UPLOAD_FOLDER):
        os.mkdir(UPLOAD_FOLDER)
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
        for file in request.files.getlist("file"):
            filename = secure_filename(file.filename)
            if filename == '':
                flash('Не выбран файл для загрузки.')
            elif file and files.is_allowed_file(filename):
                savename = "/".join([UPLOAD_FOLDER, filename])
                file.save(savename)
                files.convert_csv(savename)  # Преобразовать кодировку и прочее к нужному виду
                if os.stat(savename).st_size >= 5000000:
                    files.split_csv(savename)
            else:
                flash(
                    'Некоректное имя файла, пожалуйста не используйте в имени файла кирилические буквы и спецсимволы.')
    return render_template('upload.html', files=os.listdir(UPLOAD_FOLDER))


@app.route('/remove')
@http_auth_required
@roles_accepted('upload')
def remove_file():
    os.remove("/".join([UPLOAD_FOLDER, request.args['file']]))
    flash('Файл [%s] успешно удален с сервера.' % request.args['file'])
    return redirect('/upload')


@app.route('/to_main_db')
@http_auth_required
@roles_accepted('upload')
def send_to_main_db():
    with open("/".join([UPLOAD_FOLDER, request.args['file']]), 'r', encoding='utf-8') as file:
        fp = csv.DictReader(file, dialect='excel', delimiter=',')
        i = 0
        for row in fp:
            if (i % 5000) == 0:
                db.session.commit()
                operators = {i.login: i for i in db.session.query(Operator).all()}
                call_ids = set([i[0] for i in db.session.query(Call.id_hit).all()])
                logging.debug('Step %i ' % i)
                # i = 0
            try:
                if row['Оператор'] not in operators:
                    logging.debug('Add new operator %s', row['Оператор'])
                    new_operator = Operator(
                        login=row['Оператор'],
                        area_id=1,
                        id_=0,
                        from_=app.config['MIN_DATE'],
                        till=app.config['MAX_DATE'])
                    operators[row['Оператор']] = new_operator
                    db.session.add(new_operator)
                    row['Оператор'] = new_operator
                else:
                    row['Оператор'] = operators[row['Оператор']]
                if row['Id Хита'] not in call_ids:
                    call_ids.add(row['Id Хита'])
                    call = Call()
                    call.update_with_dict(row)
                    db.session.add(call)
            except KeyError:
                logging.debug(row)
                raise
            i += 1
        db.session.commit()
    flash('Файл [%s] успешно загружен в базу данных. Всего обработано [%i] записей.' % (request.args['file'], i))
    return redirect('/remove?file=%s' % request.args['file'])


@app.route('/get_data')
@app.route('/get_report')
@http_auth_required
def get_from_db():
    task_type = request.args.get('task_type')
    _from = datetime.strptime(request.args.get('from'), '%Y-%m-%d') if request.args.get('from') else None
    to = datetime.strptime(request.args.get('to'), '%Y-%m-%d').replace(
        hour=23, minute=59, second=59) if request.args.get('to') else None
    operators = set(request.args.get('operators').split(',')) if request.args.get('operators') else set()
    areas = set(request.args.get('areas').split(',')) if request.args.get('areas') else set()
    params = set(request.args.get('params').split(',')) if request.args.get('params') else set()
    period = request.args.get('period')
    report_id = request.args.get('report_id')
    rotate = True if request.args.get('rotate') == '1' else False
    if not report_id:
        report_id = randint(0, 1000000)
        storage.set(report_id,
                    pickle.dumps({
                        'report_id': report_id,
                        'status': 'WAIT',
                        'process_start': datetime.now()
                    }))
        main.build_report(cu.id, task_type, _from, to, operators, areas, period, params, report_id, rotate)
        time.sleep(10)
    result = pickle.loads(storage.get(report_id))
    return Response(json.dumps(result, cls=JSONEncoder), mimetype='application/json')


@app.route('/_operators', methods=['GET', 'POST'])
@app.route('/get_operators', methods=['GET', 'POST'])
@http_auth_required
def get_operators_with_area_names():
    if request.method == 'POST':
        data.change_operators(request.json)
    return Response(json.dumps(get_operators(), cls=JSONEncoder), mimetype='application/json')


@app.route('/_areas', methods=['GET', 'POST'])
@app.route('/get_areas', methods=['GET', 'POST'])
@http_auth_required
def _areas():
    if request.method == 'POST':
        data.change_areas(request.json)
    areas = db.session.query(Area)\
        .filter(Area.id.in_(cu.allowed_areas_id.split(',')) if cu.allowed_areas_id else True)\
        .order_by(Area.name)\
        .all()
    return Response(json.dumps(areas, cls=JSONEncoder), mimetype='application/json')


@app.route('/_users', methods=['GET', 'POST'])
@app.route('/get_users', methods=['GET', 'POST'])
@http_auth_required
def get_users():
    if request.method == 'POST':
        data.change_users(request.json)
    users = [i.jsonify() for i in db.session.query(User).order_by(User.email).all()]
    return Response(json.dumps(users), mimetype='application/json')


@app.route('/operators')
@http_auth_required
@roles_accepted('manage')
def manage_operators():
    if not request.script_root:
        request.script_root = url_for('index', _external=True)
    return render_template('operators.html')


@app.route('/users')
@http_auth_required
@roles_accepted('manage')
def manage_users():
    if not request.script_root:
        request.script_root = url_for('index', _external=True)
    return render_template('users.html')


# Добавление в базу таблицы утилизационной конверсии
@app.route('/converse/to_db')
@http_auth_required
@roles_accepted('upload')
def to_converse_db():
    filename = "/".join([UPLOAD_FOLDER, request.args['file']])
    month = request.args['month']
    try:
        util_converse.import_csv_to_db(filename, month)
        flash('Файл [%s] Успешно добавлен в базу данных.' % request.args['file'])
    except ValueError as e:
        flash('При обработке файла [%s] произошла ошибка. %s' % (request.args['file'], e))
        return redirect('/upload')
    # We won't delete src file if debug mode is on
    if app.debug:
        return redirect('/upload')
    return redirect('/remove?file=%s' % request.args['file'])


@app.route('/converse/get_report')
@http_auth_required
def get_converse_report():
    report_id = request.args.get('report_id')
    if not report_id:
        report_id = randint(0, 1000000)
        storage.set(report_id,
                    pickle.dumps({
                        'report_id': report_id,
                        'status': 'WAIT',
                        'process_start': datetime.now()
                    }))
        util_converse.get_report(cu.id, report_id)
        time.sleep(5)
    result = pickle.loads(storage.get(report_id))
    # result = 'bla'
    return Response(json.dumps(result, cls=JSONEncoder), mimetype='application/json')


@app.route('/converse/show')
@http_auth_required
def show_converse_report():
    if not request.script_root:
        request.script_root = url_for('index', _external=True)
    return render_template('converse.html')


# Database Services db-serv
@app.route('/db-serv/remove')
@http_auth_required
def remove_from_db():
    _from = datetime.strptime(request.args.get('from'), '%Y-%m-%d')
    till = datetime.strptime(request.args.get('till'), '%Y-%m-%d')
    flash('%i Элементов было удалено с базы данных' % (db_serv.remove_from_db(_from, till)))
    return redirect('/')
