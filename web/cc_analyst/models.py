from cc_analyst import db, app
from time import mktime
from datetime import datetime, timedelta, time, date, timezone
import json
from collections import OrderedDict
from sqlalchemy.orm.state import InstanceState
from sqlalchemy.orm import synonym
from flask_security import UserMixin, RoleMixin, current_user as cu


def fastStrptime(val: str, format: str):
    val_len = len(val)
    if format == '%d.%m.%Y %H:%M' and (val_len == 16):
        return datetime(
            int(val[6:10]),  # %Y
            int(val[3:5]),  # %m
            int(val[0:2]),  # %d
            int(val[11:13]),  # %H
            int(val[14:16]),  # %M
            0,  # %s
            0,  # %f
        )
    elif format == '%H:%M:%S' and val_len == 8:
        return time(hour=int(val[0:2]), minute=int(val[3:5]), second=int(val[6:8]))
    # Default to the native strptime for other formats.
    return datetime.strptime(val, format)


def get_operators():
    keys = ['id', 'login', 'full_name', 'id_', 'area_id', 'from', 'till', 'area_name']
    result = db.session.query(
        Operator.id, Operator.login, Operator.full_name, Operator.id_,
        Operator.area_id, Operator.from_, Operator.till, Area.name) \
        .join(Area)
    if cu.allowed_areas_id:  # Если есть ограничения по видимым регионам
        result = result.filter(Area.id.in_(cu.allowed_areas_id.split(',')))
    result = result \
        .order_by(Operator.login) \
        .all()
    result = [dict(zip(keys, i)) for i in result]
    return result


class Area(db.Model):
    __tablename__ = 'areas'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(25))
    operators = db.relationship('Operator', back_populates='area')
    _id_mark = db.Column('id_mark', db.Integer)

    @property
    def id_mark(self):
        return self._id_mark

    @id_mark.setter
    def id_mark(self, x):
        self._id_mark = int(x)

    id_mark = synonym('_id_mark', descriptor=id_mark)

    def jsonify(self):
        return {'id': self.id, 'name': self.name, 'id_mark': self.id_mark}


class Operator(db.Model):
    __tablename__ = 'operators'
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(25), index=True)
    calls = db.relationship('Call', backref='operator', lazy='dynamic')
    # util_converse = db.relationship('UtilConverse', backref='operator', lazy='dynamic')
    full_name = db.Column(db.String(100))
    _id_ = db.Column('id_', db.Integer)
    area_id = db.Column(db.Integer, db.ForeignKey('areas.id'))
    area = db.relationship('Area', back_populates='operators')
    from_ = db.Column('from', db.DateTime)
    till = db.Column(db.DateTime)

    def get_name(self):
        return '(%s) %s' % (self.id_, self.login)

    def get_area_id_mark(self):
        # return self.id_ // 1000
        # TODO Мне не нравится этот кусок кода но заказчик просил чтобы локация
        # пользователя определялась по первой цифре
        return int(str(self.id_)[:1])

    @property
    def id_(self):
        return self._id_

    @id_.setter
    def id_(self, x):
        self._id_ = int(x)

    id_ = synonym('_id_', descriptor=id_)

    def jsonify(self):
        return {
            'login': self.login,
            'full_name': self.full_name,
            'id_': self.id_,
            'area_id': self.area_id,
            'from': self.from_,
            'to': self.to
        }


class Call(db.Model):
    __tablename__ = 'calls'
    result_date = db.Column(db.DateTime, index=True)
    task_type = db.Column(db.String(50))
    id_hit = db.Column(db.String(50), primary_key=True)
    phone = db.Column(db.Integer)
    # operator = db.Column(db.String(100))
    # operator_id = db.Column(db.Integer, db.ForeignKey('operators.id'))
    operator_login = db.Column(db.String(25), db.ForeignKey('operators.login'))
    operator_skill = db.Column(db.String(100))
    que = db.Column(db.String(100))
    que_group = db.Column(db.String(100))
    result = db.Column(db.String(100))
    reason = db.Column(db.String(100))
    callback_date = db.Column(db.DateTime)
    comment = db.Column(db.String(100))
    balls = db.Column(db.String(100))
    score = db.Column(db.String(100))
    duration = db.Column(db.Interval)

    def update_with_dict(self, src):
        date_format = '%d.%m.%Y %H:%M'
        if src['Дата результата']:
            self.result_date = fastStrptime(src['Дата результата'], date_format)
        if src['Тип задания']: self.task_type = src['Тип задания']
        if src['Id Хита']: self.id_hit = src['Id Хита']
        if src['№ моб. телефона']: self.phone = src['№ моб. телефона']
        if src['Оператор']: self.operator = src['Оператор']
        if src['Скилл оператора']: self.operator_skill = src['Скилл оператора']
        if src['Очередь']: self.que = src['Очередь']
        if src['Группа очередей']: self.que_group = src['Группа очередей']
        if src['Результат']: self.result = src['Результат']
        if src['Причина']: self.reason = src['Причина']
        if src['Дата перезвона']:
            self.callback_date = fastStrptime(src['Дата перезвона'], date_format)
        if src['Комментарий']: self.comment = src['Комментарий']
        if src['Баллы']: self.balls = src['Баллы']
        if src['Оценка']: self.score = src['Оценка']
        if src['Длительность']:
            tmp = fastStrptime(src['Длительность'], '%H:%M:%S')
            self.duration = timedelta(hours=tmp.hour, minutes=tmp.minute, seconds=tmp.second)

    def to_dict(self):
        res = OrderedDict()
        res['Дата результата'] = self.result_date.date()
        res['Время результата'] = self.result_date.time() \
            if self.result_date else None
        res['Тип задания'] = self.task_type
        res['Id Хита'] = self.id_hit
        res['№ моб. телефона'] = self.phone
        res['Оператор'] = self.operator
        res['Скилл оператора'] = self.operator_skill
        res['Очередь'] = self.que
        res['Группа очередей'] = self.que_group
        res['Результат'] = self.result
        res['Причина'] = self.reason
        res['Дата перезвона'] = self.callback_date.date() \
            if self.callback_date else None
        res['Время перезвона'] = self.callback_date.time() \
            if self.callback_date else None
        res['Комментарий'] = self.comment
        res['Баллы'] = self.balls
        res['Оценка'] = self.score
        res['Длительность'] = self.duration
        return res


class JSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return int(obj.replace(tzinfo=timezone.utc).timestamp())
            # return int(mktime(obj.timetuple()))
        if isinstance(obj, time):
            return str(obj)
        if isinstance(obj, date):
            return str(obj)
        if isinstance(obj, timedelta):
            return str(obj)
        if isinstance(obj, Operator):
            return obj.__dict__
        if isinstance(obj, Area):
            return obj.jsonify()
        if isinstance(obj, set):
            return list(obj)
        if isinstance(obj, InstanceState):
            return ''
        return json.JSONEncoder.default(self, obj)


# Define models
roles_users = db.Table('roles_users',
                       db.Column('user_id', db.Integer(), db.ForeignKey('users.id')),
                       db.Column('role_id', db.Integer(), db.ForeignKey('roles.id')))


class Role(db.Model, RoleMixin):
    __tablename__ = 'roles'
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))


class User(db.Model, UserMixin):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    allowed_areas_id = db.Column(db.String(50))
    roles = db.relationship('Role', secondary=roles_users, backref=db.backref('users', lazy='dynamic'))

    def jsonify(self):
        return {
            'id': self.id,
            'log': self.email,
            'pwd': self.password,
            'allowed_areas_id': self.allowed_areas_id.split(',') if self.allowed_areas_id else []
        }


# Таблица util_converse
class UtilConverse(db.Model):
    __tablename__ = 'util_converse'
    agent_login = db.Column(db.String(25), primary_key=True)
    successed = db.Column(db.Integer)
    utilized = db.Column(db.Integer)
    recall_rate = db.Column(db.Float(precision=2))
    util_finish_30 = db.Column(db.Float(precision=2))
    last_updated = db.Column(db.DateTime)
    month = db.Column(db.DateTime)
    __table_args__ = (db.ForeignKeyConstraint(['agent_login'], ['operators.login'],
                                              name='fk_utilconverse_operators'), )

    def _school_round(self, number, precision):
        # TODO: Move it to gen_func Module
        """ Реализует "школьное" округление чисел

        Args:
            number (TYPE): Число, которое нужно окргулить
            precision (TYPE): Количество знаков после запятой
        """
        from decimal import Decimal, ROUND_HALF_UP
        return float(Decimal(number * 10 ** precision).quantize(Decimal(1), rounding=ROUND_HALF_UP)) / 10 ** precision

    def is_data_bad(self):
        if not self.successed:
            if self.util_finish_30:
                return 'Для оператора %s утиль должен равнятся 0 а не %f' % (self.agent_login, self.util_finish_30)
            else:
                return False
        calc = self._school_round(self.utilized * 100 / self.successed, 2)
        if self.util_finish_30 != calc:
            return 'Для оператора %s утиль должен равнятся %f а не %f' % (self.agent_login, calc, self.util_finish_30)
        return False
