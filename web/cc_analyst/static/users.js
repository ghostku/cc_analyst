function show_users(users){
	console.log(users)
	for (i in users){
		var tr = $('<tr></tr>')
		tr.append($('<td></td>').text(users[i].log).attr('col', 'log'));
		tr.append($('<td></td>').text(users[i].pwd).attr('col', 'pwd'));
		var select = $('<select class="selector_select_2" multiple="multiple" style="width: 100%"></select>')
		
		selected = users[i].allowed_areas_id
		if (selected) {

		for (j in selected){
			select.append($('<option value="'+selected[j]+'" selected="selected">'+JSON.parse(sessionStorage.areas_dict)[selected[j]]+'</option>'))
		}
	}
		// tr.append(select)
		tr.append($('<td></td>').append(select))
		tr.append($('<button type="button" class="btn btn-danger btn-xs rm-row"><span class="glyphicon glyphicon-trash"></span></button>')) //.attr('id', users[i].id))//.text('Удалить'))
		
		$('#users').append(tr.attr('id', users[i].id).attr('modify', users[i].modify))
	}
	$('.selector_select_2').select2({data: JSON.parse(sessionStorage.areas_list), placeholder: 'При пустом поле - разрешены все локации'})


	$(".rm-row").on('click', function(event){
		event.stopPropagation();
		event.stopImmediatePropagation();
		$(this).closest('tr')
			.hide()
			.attr('modify', 'remove')
    });
	$('#users').editableTableWidget();
    $('#users td').on('change', function(evt, newValue){
    	$(this).closest('tr').attr('modify', 'modify');
    });
}

function add_user(){
	show_users([{'log':'Логин','allowed_areas_id': null,'id':0,'modify':'new'}]);
}

function save_users(){
	var result = []
	$('#users > tbody > tr').each(function() {
		var record = {
			'id': $(this).attr('id'),
			'modify': $(this).attr('modify')
		}
		$(this).find('td').each(function() {
			var $th = $(this).closest('table').find('th').eq($(this).index());
			if ($th.attr('field-type') == 'select'){
				record[$th.attr('field-name')] = $(this).find('select').val();
			}
			else {
				record[$th.attr('field-name')] = $(this).text()
			}
		})
		result.push(record);
	})
	$.ajax({
		type: 'POST',
		contentType: "application/json; charset=utf-8",
		url: $SCRIPT_ROOT + '_users',
		dataType : 'json',
		data: JSON.stringify(result),
		success: function(data) {
			$('#users tbody').empty()
			show_users(data);
		},
		error: function() {
			alert('Some sheet happens')
		}
	});
}