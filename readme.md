#CC Analyst

##Installation

1 Set time zone
sudo timedatectl set-timezone Europe/Kiev

2 Install Redis

3 Install Python
sudo apt-get update
sudo apt-get install apache2 python3-dev libapache2-mod-wsgi-py3
sudo a2enmod wsgi
sudo apt-get install python3-pip

sudo pip3 install Flask sqlalchemy flask-sqlalchemy sqlalchemy-migrate flask_migrate flask_security flask-redis
python3-dateutil

## Naming

**ucon** - Utilisation Converse

## TODO
- DONE: Решить вопрос с кодировкой загружаемых файлов
- TODO: Обработать ошибку "Нет Данных"
- TODO: Отсортировать файлы при выводе
- DONE: Удалять файлы при успешном добавлении в базу
- TODO: Сделать возврат и обработку ошибок
- TODO: Прокоментировать весь код
- DONE: Невыводить операторов у которых не было смен за период
- TODO: Переписать связь User to Areas
- TODO: Посмотреть почему мы не считаем время выполнения для периодов не равных дню и возможно исправить если можно
- TODO: Переписать систему работы с конфигами как в FB_Analizer
- TODO: Переехать на Gunicorn и nginx
- TODO: Переехать на PostgreSQL
- TODO: Завернуть все в Docker

- TODO: Сделать возможность менять код оператора в зависимости от даты
COMMENT

- cc_analyst
 	- [x] __init__.py
 	- [x] data.py
	- [ ] decorators.py
 	- [ ] files.py
 	- [ ] main.py
 	- [ ] models.py
 	- [ ] views.py