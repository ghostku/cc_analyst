import logging
from datetime import datetime
from cc_analyst import db, models


def change_areas(data):
    """ Обновляет данные о локациях на основании полученного списка локаций.
    Если для элемента списка выставлена метка 'modify' в значение:
        - 'remove' то элемент нужно удалить
        - 'modify' то элемент нужно изменить если он уже существует или создать новый если не существует
    Для локации обновляются следующие поля:
        - 'name': Название локации
        - 'id_mark': Число с которого начинаются АйДи принадлежащих локации операторов

    Args:
        data (list): Список локаций

    """
    areas = {i.id: i for i in db.session.query(models.Area).all()}
    for record in data:
        id = int(record['area_id'])
        if record.get('modify') == 'remove':
            logging.info('Remove Area %s', record['name'])
            db.session.delete(areas[id])
        elif record.get('modify') == 'modify':
            logging.info('Modify|Add Area %s', record['name'])
            area = areas.get(id, models.Area(name='', id_mark=0))
            area.name = record['name']
            area.id_mark = record['id_mark']
            logging.debug(area)
            db.session.add(area)
    db.session.commit()


def change_operators(data):
    """Обновляет данные о операторах на основании полученного списка операторов.
    Если для элемента списка выставлена метка 'modify' в значение:
        - 'remove' то элемент нужно удалить (Сейчас отключено)
        - 'modify' то элемент нужно изменить если он уже существует или создать новый если не существует
    Для оператора обновляются следующие поля:
        - 'full_name': Полное имя оператора
        - 'id_': АйДи оператора
        - 'area_id': Локация к которой принадлежит оператор, пересчитывается автоматически на основании id_
    Args:
        data (list): Список операторов

    """
    operators = {i.id: i for i in db.session.query(models.Operator).all()}
    areas = {i.id_mark: i.id for i in db.session.query(models.Area).all()}
    for record in data:
        id = int(record['id'])
        if record.get('modify') == 'remove':
            logging.info('Remove Operator %s', record['id'])
            # db.session.delete(operators[id])
        elif record.get('modify') == 'update':
            logging.info('Update|Add Operator %s', record['id'])
            operator = operators.get(id, models.Operator(login='', area_id=0))

            # Modify Operator
            operator.full_name = record['full_name']
            operator.id_ = int(record['id_'])
            operator.area_id = areas.get(operator.get_area_id_mark(), 1)
            if 'from' in record:
                operator.from_ = datetime.fromtimestamp(record['from'])
            if 'till' in record:
                operator.till = datetime.fromtimestamp(record['till'])

            if 'from' in record:
                pass
                # Найти ближайший запись
                #  Если все ок то изменить в ней till
                #  
            operators[id] = operator
            db.session.add(operator)
    # Проверка на валидность дат
    
    db.session.commit()


def change_users(data):
    """Обновляет данные о пользователях на основании полученного списка пользователей.
    Если для элемента списка выставлена метка 'modify' в значение:
        - 'remove' то элемент нужно удалить
        - 'modify' то элемент нужно изменить если он уже существует или создать новый если не существует
    Для пользователя обновляются следующие поля:
        - 'email': Логин
        - 'password': Пароль
        - 'allowed_areas_id': Список локаций к которым имеет доступ пользователь
    Args:
        data (list): Список операторов

    """
    users = {i.id: i for i in db.session.query(models.User).all()}
    for record in data:
        id = int(record['id'])
        if record.get('modify') == 'remove':
            logging.info('Remove User %s', record['log'])
            db.session.delete(users[id])
        elif record.get('modify') == 'modify':
            logging.info('Modify|Add User %s', record['log'])
            user = users.get(id, models.User())
            user.email = record['log']
            user.password = record['pwd']
            user.allowed_areas_id = ','.join(record['allowed_areas_id'])
            db.session.add(user)
    db.session.commit()
