import csv
import pickle
import logging
from datetime import datetime
from cc_analyst.decorators import async
from cc_analyst import db, storage
from cc_analyst.models import UtilConverse, User, Operator, Area

CSV_HEADERS = ['oper_id', 'Оформленные', 'Утилизированные', 'Перезвоны', 'Утиль']


def import_csv_to_db(filename, month):
    """ Import CSV file with util converse to DB

    Args:
        filename (TYPE): Path to CSV file
        month (TYPE): Month of all records in file like 'YYYY-MM'

    Returns:
        TYPE: None

    Raises:
        ValueError: In case of any errors
    """
    with open(filename, 'r', encoding='utf-8') as file:
        fp = csv.DictReader(file, dialect='excel', delimiter=',')
        if CSV_HEADERS != fp.fieldnames:
            raise ValueError('Названия колонок в файле "%s" не сответствуют эталону "%s". Возможно это не тот файл' %
                             (' | '.join(fp.fieldnames), ' | '.join(CSV_HEADERS)))
        for row in fp:
            new_record = UtilConverse(
                agent_login=row['oper_id'],
                successed=int(row['Оформленные']),
                utilized=int(row['Утилизированные']),
                recall_rate=float(row['Перезвоны'].strip('%')) if row['Перезвоны'] != '.' else 0.0,
                util_finish_30=float(row['Утиль'].strip('%')) if row['Утиль'] != '.' else 0.0,
                last_updated=datetime.now(),
                month=datetime.strptime(month, '%Y-%m'))
            is_data_bad = new_record.is_data_bad()
            if is_data_bad:
                raise ValueError(is_data_bad)
            # TODO: Add 'Total' row check and filter it
            db.session.merge(new_record)
        db.session.commit()
    return


def build_report(user):
    result = []
    u_conv = db.session.query(UtilConverse, Area) \
        .join(Operator, Area)
    if user.allowed_areas_id:
        u_conv = u_conv.filter(Area.id.in_(user.allowed_areas_id))
    u_conv = [i for i in u_conv]
    logging.debug('Всего выгружено %i элементов.' % len(u_conv))
    for item in u_conv:
        result.append({
            'agent_login': item[0].agent_login,
            'recall_rate': item[0].recall_rate,
            'util_finish_30': item[0].util_finish_30,
            'last_updated': item[0].last_updated,
            'successed': item[0].successed,
            'utilized': item[0].utilized,
            'area': item[1].name,
            'month': item[0].month.strftime('%Y-%m'),
        })
    return result


@async
def get_report(user_id, report_id):
    data = {'status': 'DONE', 'report_id': report_id, 'process_start': datetime.now(), 'request': {}}
    user = db.session.query(User).filter(User.id == user_id).first()
    data['response'] = build_report(user)
    storage.set(report_id, pickle.dumps(data))
    return
