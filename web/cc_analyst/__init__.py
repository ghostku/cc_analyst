import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_redis import FlaskRedis
from flask_migrate import Migrate

app = Flask(__name__)
app.config.from_object(os.environ.get('CC_ANALYST', 'config.DevelopmentConfig'))
# app.config.from_object('config.ProductionConfig')
app.secret_key = 'some_secret'
db = SQLAlchemy(app)
migrate = Migrate(app, db)
storage = FlaskRedis(app)

from cc_analyst import views, models, security
