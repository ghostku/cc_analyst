# Excel Functons
# 2016-05-08 Initial commit
def convert_to_xls(src_filename,target_filename): # Открывает средствами Excel любой файл который модет открыть Excel и сохраняет его как *.xls
	import win32com.client as win32
	import os
	excel = win32.gencache.EnsureDispatch('Excel.Application')
	name, ext = os.path.splitext(src_filename)
	wb = excel.Workbooks.Open(src_filename)
	excel.DisplayAlerts = False
	wb.DoNotPromptForConvert = True
	wb.CheckCompatibility = False
	wb.SaveAs(target_filename, FileFormat=56, ConflictResolution=2)
	excel.Application.Quit()

def xlsx_to_dict(filename): # Вычитывает xlsx файл в словарь. Вычитывает только первый лист.
	''' Convert any file that could be opened пше і'''
	from openpyxl import load_workbook
	wb = load_workbook(filename)
	ws = wb.active
	res = [[j.value for j in i] for i in ws.iter_rows()]
	header = res.pop(0)
	res = list(map(lambda x:dict(zip(header,x)),res))
	return res