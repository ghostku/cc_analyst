# Define the parameters for a specific virtual host/server
server {
    # Define the directory where the contents being requested are stored
    # root /usr/src/app/project/;

    # Define the default page that will be served If no page was requested
    # (ie. if www.kennedyfamilyrecipes.com is requested)
    # index index.html;

    # Define the server name, IP address, and/or port of the server
    listen 80;
    listen [::]:80;
    server_name bot.ghostku.com

    # Define the specified charset to the “Content-Type” response header field
    charset utf-8;

    # Configure NGINX to deliver static content from the specified folder
    # location /static {
    #     alias /usr/src/app/web/project/static;
    # }

    # Getting Let's Encrypt certificate
    location ^~ /.well-known/acme-challenge {
        root   /usr/share/nginx/html;
        default_type text/plain;
        allow all;
    }

    location / {
        rewrite ^ https://$host$request_uri? permanent;
    }
}
# https://bot.ghostku.com
server {

    # Define the server name, IP address, and/or port of the server
    listen 443 ssl;
    # listen [::]:443;
    server_name bot.ghostku.com;

    # SSL
    # server_tokens off;
    # ssl on;

    # ssl_certificate /etc/letsencrypt/live/bot.ghostku.com/fullchain.pem;
    # ssl_certificate_key /etc/letsencrypt/live/bot.ghostku.com/privkey.pem;

    ssl_certificate /etc/self/cert.pem;
    ssl_certificate_key /etc/self/pkey.pem;

    # ssl_buffer_size 8k;

    # ssl_dhparam /etc/ssl/certs/dhparam-2048.pem;

    ssl_protocols TLSv1.2 TLSv1.1 TLSv1;
    ssl_prefer_server_ciphers on;

    ssl_ciphers ECDH+AESGCM:ECDH+AES256:ECDH+AES128:DH+3DES:!ADH:!AECDH:!MD5;

    ssl_ecdh_curve secp384r1;
    # ssl_session_tickets off;

    # OCSP stapling
    # ssl_stapling on;
    # ssl_stapling_verify on;
    # resolver 8.8.8.8;

    # Define the specified charset to the “Content-Type” response header field
    # charset utf-8;

    # Getting Let's Encrypt certificate
    location ^~ /.well-known/acme-challenge {
        root   /usr/share/nginx/html;
        default_type text/plain;
        allow all;
    }

    # Configure NGINX to reverse proxy HTTP requests to the upstream server (Gunicorn (WSGI server))
    location / {
        # Define the location of the proxy server to send the request to
        proxy_pass http://web:8000;
        proxy_redirect off;

        # Redefine the header fields that NGINX sends to the upstream server
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Host $server_name;

        # Define the maximum file size on file uploads
        client_max_body_size 5M;
        client_body_buffer_size 5M;
    }
}
