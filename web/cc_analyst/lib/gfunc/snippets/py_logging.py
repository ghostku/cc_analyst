# logging.py
import logging
levels = {
    'CRITICAL': logging.CRITICAL,
    'ERROR': logging.ERROR,
    'WARNING': logging.WARNING,
    'INFO': logging.INFO,
    'DEBUG': logging.DEBUG
}
try:
    logging.basicConfig(level=levels[sys.argv[-1]])
except (IndexError, KeyError):
    logging.basicConfig(level=logging.WARNING)
