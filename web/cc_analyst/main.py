from sqlalchemy import and_
from cc_analyst import models, db, storage
from dateutil.relativedelta import relativedelta
from datetime import timedelta, datetime
from calendar import monthrange
import logging
from collections import OrderedDict
import pickle
from cc_analyst.decorators import async
from collections import defaultdict

from datetime import datetime

recursivedict = lambda: defaultdict(recursivedict)

NORM_HOURS_IN_PAIR = 6


class Period(object):
    """docstring for Period"""

    def __init__(self, start, end, period='day', name='TEST'):
        super(Period, self).__init__()
        self.name = name
        self.period = period
        if self.period == 'day':
            self.start = start
            self.end = end
        elif self.period == 'month':
            self.start = start.replace(day=1)
            last_day_in_month = monthrange(end.year, end.month)[1]
            self.end = end.replace(day=last_day_in_month)
        else:
            raise ValueError('"{}" can not be a period value'.format(self.period))

    def generate_name(self, period, step):
        start = self.start.date()
        end = self.end.date()
        if step > 1:
            self.name = '%s - %s' % (start, end)
        elif period == 'day':
            self.name = '%s' % (start.strftime('%d.%m.%Y'))
        elif period == 'month':
            self.name = start.strftime('%Y %b')
        return self

    def split(self, step):
        one_day = relativedelta(days=1)
        if self.period == 'day':
            delta = relativedelta(days=step)
        elif self.period == 'month':
            delta = relativedelta(months=step)
        start = self.start
        end = self.end

        periods = []
        while start <= end - delta:
            next_date = start + delta
            periods.append(Period(start, next_date - one_day))
            start = next_date
        if start <= end:
            periods.append(Period(start, end))

        for i in periods:
            i.end = i.end.replace(hour=23, minute=59, second=59)
            i.generate_name(self.period, step)
        return periods

    def days(self):
        return (self.end - self.start).days

    def contains(self, date):
        """Проверяет попадает ли date в данный период

        Args:
            date (datetime): Дата, которую нужно проверить

        Returns:
            bool: True - если дата попадает в период False - если не попадает.

        """
        if (date >= self.start) and (date <= self.end):
            return True
        return False


class Data_Per_Period(object):
    """docstring for Data_Per_Period"""
    field_names = [
        'calls_succes_ok',
        'pairs',
        'calls_all',
        'calls_succes_all',
        'ok_per_pair',
        'prk_ok',
        'KPI_succes_reject',
        'KPI_succes_ok',
        'av_calls_all_per_hour',
        'av_calls_ok_per_hour',
        'KPI_all',
        'calls_succes_reject',
        'KPI_all_reject',
        'calls_failed',
        'KPI_all_failed',
        'calls_succes_callback',
        'KPI_all_callback',
        'calls_succes_hold',
        'KPI_all_hold',
        'calls_succes_ok',
        'KPI_all_ok',
        # 'KPI_all_succes',
        # 'ok_to_calls_all',
        'KPI_succes',
        'calls_succes_reject',
        'KPI_succes_reject',
        'calls_succes_callback',
        'KPI_succes_callback',
        'calls_succes_hold',
        'KPI_succes_hold',
        'calls_succes_ok',
        'KPI_succes_ok',
        'av_call_time',
        'av_call_time_one_abonent',
        'av_call_time_reject',
        'av_call_time_ok',
        'pair_start',
        'pair_end']

    def __init__(self, calls=[], period=None, operators_qnty=1):
        super(Data_Per_Period, self).__init__()
        self.operators_qnty = len(set([i.operator_login for i in calls]))
        self.one_day = period and period.days() < 1  # Если период составлят один день
        self.calls = calls

        self.calls_all = 0
        self.calls_succes_ok = 0
        self.calls_succes_callback = 0
        self.calls_failed = 0
        self.calls_succes_reject = 0
        self.calls_succes_hold = 0
        self.calls_succes_all = 0
        self.pairs = 0
        self.pair_start = ''
        self.pair_end = ''
        self.ok_per_pair = None
        self.prk_ok = None
        self.ok_to_calls_all = None
        self.KPI_succes = None
        self.KPI_succes_callback = None
        self.KPI_succes_reject = None
        self.KPI_succes_hold = None
        self.KPI_succes_ok = None
        self.KPI_all = None
        self.KPI_all_succes = None
        self.KPI_all_callback = None
        self.KPI_all_failed = None
        self.KPI_all_reject = None
        self.KPI_all_hold = None
        self.KPI_all_ok = None
        self.av_calls_all_per_hour = None
        self.av_calls_ok_per_hour = None
        self.av_call_time = None
        self.av_call_time_reject = None
        self.av_call_time_ok = None
        self.av_call_time_one_abonent = None

    def add(self, second):
        self.calls_all += second.calls_all
        self.calls_succes_ok += second.calls_succes_ok
        self.calls_succes_callback += second.calls_succes_callback
        self.calls_failed += second.calls_failed
        self.calls_succes_reject += second.calls_succes_reject
        self.calls_succes_hold += second.calls_succes_hold
        self.calls_succes_all += second.calls_succes_all
        self.pairs += second.pairs

    def calculate_main(self):
        if self.calls:
            self.calls_all = len(self.calls)
            self.calls_succes_ok = sum(1 if x.result == 'Дозвон, Успешно' else 0 for x in self.calls)
            self.calls_succes_callback = sum(1 if x.result == 'Дозвон, Перезвонить' else 0 for x in self.calls)
            self.calls_failed = sum(1 if x.result in ['Недозвон', 'Не было звонка'] else 0 for x in self.calls)
            self.calls_succes_reject = sum(1 if x.result == 'Дозвон, Отказ' else 0 for x in self.calls)
            self.calls_succes_hold = sum(1 if x.result == 'Дозвон, Отложить' else 0 for x in self.calls)
            self.calls_succes_all = sum(1 if 'Дозвон' in x.result else 0 for x in self.calls)
            self.pairs = len(set([i.result_date.date() for i in self.calls])) * self.operators_qnty

    def calculate_rest(self):
        # Выходим если не было смен в период
        if self.pairs == 0:
            return
        if self.one_day and self.operators_qnty == 1:
            self.pair_start = min([i.result_date for i in self.calls]).time()
            self.pair_end = max([i.result_date + i.duration for i in self.calls]).time()
        self.ok_per_pair = self.calls_succes_ok / self.pairs
        try:
            self.prk_ok = self.calls_succes_ok / (self.calls_succes_ok + self.calls_succes_reject)
        except ZeroDivisionError:
            self.prk_ok = '-'

        if self.calls_succes_all:
            self.ok_to_calls_all = self.calls_succes_ok / self.calls_succes_all
            self.KPI_succes = None
            self.KPI_succes_callback = self.calls_succes_callback / self.calls_succes_all
            self.KPI_succes_reject = self.calls_succes_reject / self.calls_succes_all
            self.KPI_succes_hold = self.calls_succes_hold / self.calls_succes_all
            self.KPI_succes_ok = self.calls_succes_ok / self.calls_succes_all

        self.KPI_all = None
        self.KPI_all_succes = self.calls_succes_all / self.calls_all
        self.KPI_all_callback = self.calls_succes_callback / self.calls_all
        self.KPI_all_failed = self.calls_failed / self.calls_all
        self.KPI_all_reject = self.calls_succes_reject / self.calls_all
        self.KPI_all_hold = self.calls_succes_hold / self.calls_all
        self.KPI_all_ok = self.calls_succes_ok / self.calls_all

        hours = self.pairs * NORM_HOURS_IN_PAIR
        # hours = ceil((end - start).total_seconds() / (60 * 60))

        self.av_calls_all_per_hour = self.calls_all / hours
        self.av_calls_ok_per_hour = self.calls_succes_all / hours

        self.av_call_time = (lambda x: x - timedelta(microseconds=x.microseconds))(
            sum([i.duration for i in self.calls], timedelta()) / self.calls_all)

        self.av_call_time_reject = (lambda x: x - timedelta(microseconds=x.microseconds))(
            sum([i.duration for i in self.calls if i.result == 'Дозвон, Отказ'], timedelta()) / self.calls_succes_reject) \
            if self.calls_succes_reject else '-'

        self.av_call_time_ok = (lambda x: x - timedelta(microseconds=x.microseconds))(
            sum([i.duration for i in self.calls if i.result == 'Дозвон, Успешно'], timedelta()) / self.calls_succes_ok) \
            if self.calls_succes_ok else '-'

        return self

    def export_data(self, params=None):
        """Возвращает посчитанные параметры обьекта. Если `params` пуст - то
        возвращаются все параметры иначе, только те которые перечислены в `params`

        Args:
            params (list): Список параметров которые нужно получить

        Returns:
            dict: Словарь параметров вида {'param_name': 'value'}
        """
        export = OrderedDict()
        self_dict = self.__dict__
        items = params if params else self.field_names
        for item in items:
            try:
                export[item] = self_dict[item]
            except KeyError:
                continue
        return export


def _rotate_table(src):
    rotated = OrderedDict()
    for i in list(src.values()).pop():
        rotated[i] = OrderedDict()

    for k1, v1 in src.items():
        for k2, v2 in v1.items():
            rotated[k2][k1] = v2

    return rotated

# TODO Сделать так чтобы JOIN делался только если нужно

def by_operator(user, period, logins, areas, step, params):
    periods = period.split(step)
    total_data = Data_Per_Period()
    result = OrderedDict({'ВСЕГО': 0})
    for period in periods:
        logging.debug('[%s] From %s to %s for %s', period.name, period.start, period.end, ', '.join(logins))
        calls = db.session.query(models.Call) \
            .join(models.Operator, models.Area) \
            .filter(models.Call.result_date.between(period.start, period.end)) \
            .filter(models.Call.result_date.between(models.Operator.from_, models.Operator.till))
        if  user.allowed_areas_id:
            calls = calls.filter(models.Area.id.in_(user.allowed_areas_id))
        if logins:
            calls = calls.filter(models.Operator.login.in_(logins))
        if areas:
            calls = calls.filter(models.Area.id.in_(areas))
        calls = calls.order_by(models.Call.result_date)
        calls = [i for i in calls]
        logging.debug('Всего выгружено %i элементов.' % len(calls))
        data = Data_Per_Period(calls, period, len(logins))
        if len(calls):  # Если данных за этот период не было - то переходим к следующему
            data.calculate_main()
            data.calculate_rest()
        result[period.name] = data.export_data(params)
        total_data.add(data)
    total_data.calculate_rest()
    result['ВСЕГО'] = total_data.export_data(params)
    return result


def by_period(user, period, logins, areas, params):
    result = OrderedDict()
    tmp = recursivedict()
    operators = {i.login: i for i in db.session.query(models.Operator).all()}
    if not logins:
        logins = [i.login for i in db.session.query(models.Operator).all()]
    calls = db.session.query(models.Call) \
        .join(models.Operator, models.Area) \
        .filter(and_(models.Call.result_date >= period.start, models.Call.result_date <= period.end)) \
        .filter(models.Area.id.in_(user.allowed_areas_id) if user.allowed_areas_id else True) \
        .filter(models.Call.result_date.between(models.Operator.from_, models.Operator.till))
    if areas:
        calls = calls.filter(models.Area.id.in_(areas))
    calls = calls.order_by(models.Call.result_date)
    for call in calls:
        tmp[call.operator_login] = tmp.get(call.operator_login, []) + [call]

    for login in logins:
        calls = tmp.get(login, [])
        if not calls:  # Если данных за этот период не было - то переходим к следующему
            continue
        logging.debug('[%s] From %s to %s for %s', period.name, period.start, period.end, login)
        data = Data_Per_Period(calls, period, 1)
        data.calculate_main()
        data.calculate_rest()
        result[operators[login].get_name()] = data.export_data(params)
    return result


def by_param(user, period, logins, areas, step, params):
    """Create report for one or some parameters
    Where colums are periods and Rows are operators or vice versa
    
    Args:
        period (Period): Период по которому мы строим отчет
        logins (TYPE): Description
        step (TYPE): Description
        params (TYPE): Description
    
    Returns:
        TYPE: Description
    
    Deleted Parameters:
        start (TYPE): Description
        end (TYPE): Description
        period_type (TYPE): Description
    """
    periods = period.split(step)
    result = OrderedDict()
    calls_all = recursivedict()
    operators = {i.login: i for i in db.session.query(models.Operator).all()}
    if not logins:
        logins = [i.login for i in db.session.query(models.Operator).all()]
    calls = db.session.query(models.Call) \
        .join(models.Operator, models.Area) \
        .filter(and_(models.Call.result_date >= period.start, models.Call.result_date <= period.end)) \
        .filter(models.Operator.login.in_(logins)) \
        .filter(models.Area.id.in_(user.allowed_areas_id) if user.allowed_areas_id else True) \
        .filter(models.Call.result_date.between(models.Operator.from_, models.Operator.till))
    if areas:
        calls = calls.filter(models.Area.id.in_(areas))
    calls = calls.order_by(models.Call.result_date)
    periods_iter = iter(periods)
    period = next(periods_iter)
    for call in calls:
        while not period.contains(call.result_date):
            period = next(periods_iter)
        calls_all[call.operator.login][period.name] = calls_all.get(call.operator.login, {}).get(period.name, []) + [call]
    for login in logins:
        calls_this_login = calls_all.get(login, {})
        if not calls_this_login:  # Если у оператора не было звонков в запрашиваемом периоде - пропускаем оператора
            continue
        result[operators[login].get_name()] = OrderedDict({'ВСЕГО': 0})
        total_data = Data_Per_Period()
        for period in periods:
            logging.debug('[%s] From %s to %s for %s', period.name, period.start, period.end, login)
            calls = calls_this_login.get(period.name, [])
            data = Data_Per_Period(calls, period, 1)
            if len(calls):  # Если данных за этот период не было - то переходим к следующему
                data.calculate_main()
                data.calculate_rest()
            result[operators[login].get_name()][period.name] = data.export_data(params)
            total_data.add(data)
        total_data.calculate_rest()
        result[operators[login].get_name()]['ВСЕГО'] = total_data.export_data(params)
    return result


@async
def build_report(user_id, task_type, _from, to, operators, areas, period_type, params, report_id, rotate):
    data = {
        'status': 'DONE',
        'report_id': report_id,
        'process_start': datetime.now(),
        # 'user': user.name,
        'request': {
            'task_type': task_type,
            'from': str(_from),
            'to': str(to),
            'operators': operators,
            'areas': areas,
            'period': period_type,
            'param': params
        }
    }
    user = db.session.query(models.User).filter(models.User.id == user_id).first()
    data['user'] = user.email
    period = Period(_from, to, period_type)
    try:
        if task_type == 'all_data':
            logging.debug('Get all records from: %s to: %s', _from, to)
            data['response'] = [i.to_dict() for i in db.session.query(models.Call).filter(models.Call.result_date.between(_from, to))]

        elif task_type == 'by_operator':
            logging.debug('Get all records from: %s to: %s for operator %s with period in %s', _from, to, operators, period)
            data['response'] = by_operator(user, period, operators, areas, 1, params)

        elif task_type == 'by_period':
            logging.debug('Compare operators %s in period from: %s to: %s', operators, _from, to)
            data['response'] = by_period(user, period, operators, areas, params)

        elif task_type == 'by_param':
            logging.debug('Compare operators %s in period from: %s to: %s by parameter %s', operators, _from, to, params)
            data['response'] = by_param(user, period, operators, areas, 1, params)
        else:
            data['status'] = 'ERROR'

        if rotate:
            logging.debug('rotation')
            data['response'] = _rotate_table(data['response'])

    except AttributeError:
        data['status'] = 'ERROR'
        storage.set(report_id, pickle.dumps(data))
        raise
    # logging.log('[%i] Task done Saving data.' % data['report_id'])
    data['process_duration'] = datetime.now() - data['process_start']
    storage.set(report_id, pickle.dumps(data))
