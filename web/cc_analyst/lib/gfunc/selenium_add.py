from selenium import webdriver
from selenium.common.exceptions import *
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def get_driver(myProxy='',profile=''):
	""" This is a help function that returns firefox webdriver object.
	Maybe in future I'll replace it with snippet
	
	Args:
	    myProxy (str, optional): Proxy settings IP:Port e.g. `127.0.0.1:8080`
	    profile (str, optional): Path to FireFox profile folder
	
	Returns:
	    webdriver.object: selenium.webdriver
	"""

	from selenium.webdriver.common.proxy import ProxyType, Proxy
	proxy = Proxy({
	    'proxyType': ProxyType.MANUAL,
	    'httpProxy': myProxy,
	    'ftpProxy': myProxy,
	    'sslProxy': myProxy,
	    'noProxy': '' # set this value as desired
	    })
	if profile:
		driver = webdriver.Firefox(webdriver.FirefoxProfile(profile),proxy=proxy)
	else:
		driver = webdriver.Firefox(proxy=proxy)
	return driver

from selenium.common.exceptions import NoSuchElementException
def check_exists_by_xpath(xpath,d):
    try:
        d.find_element_by_xpath(xpath)
    except NoSuchElementException:
        return False
    return True