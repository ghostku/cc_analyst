# coding=utf-8
import csv
import os
from itertools import count
from cc_analyst import app
import logging

def split_csv(src_filename):
    """ Делит исходный csv файл на меньший файлы с количеством обьектов
    непревышающим MAX_CALLS_PER_FILE
        Результирующие файлы создаются в той же папке что и исходный
    и по имени отличются добавленым счётчиком _0

    Args:
        src_filename (str): Полный путь к исходному файлу

    Returns:
        bool: 0 - если все хорошо
    """
    name, extension = src_filename.rsplit('.', 1)
    with open(src_filename, 'r', encoding='utf-8') as src_file:
        src_fp = csv.DictReader(src_file, dialect='excel', delimiter=',')
        header = src_fp.fieldnames
        i = count(0, 1)
        k = count(0, 1)
        for row in src_fp:
            if (next(i) % app.config['MAX_CALLS_PER_FILE']) == 0:
                dst_file = open(name + '_' + str(next(k)) + '.' + extension, 'w', encoding='utf-8', newline='\n')
                dst_fp = csv.DictWriter(dst_file, header, dialect='excel', delimiter=',')
                dst_fp.writeheader()
            # del row[None]
            dst_fp.writerow(row)
    os.remove(src_filename)
    return 0


def convert_csv(filename):
    """ Преобразовывает csv файл у более стандартному виду
    - Меняет кодировку файла с INCOMING_ENCODING на utf-8
    - Убирает строку sep= в начале файла

    Args:
        filename (str): Имя исходного файла

    Returns:
        bool: 0 - если все хорошо
    """
    import shutil
    DELIMITERS = ';,'
    SRC_ENCODING = app.config['INCOMING_ENCODING']
    DST_ENCODING = 'utf-8'
    DST_DELIMITER = ','

    filename_tmp = filename + '.tmp'

    src = open(filename, 'r', encoding=SRC_ENCODING)
    dst = open(filename_tmp, 'w', encoding=DST_ENCODING, newline='\n')
    # Опеределим тип CSV файла
    src_dialect = csv.Sniffer().sniff(src.read(1024), delimiters=DELIMITERS)
    src.seek(0)
    first_line = src.readline()
    if first_line.startswith('sep=,'):
        pass
    else:
        src.seek(0, 0)
    csv_src = csv.reader(src, src_dialect)
    csv_dst = csv.writer(dst, delimiter=DST_DELIMITER)
    for row in csv_src:
        if any(row):  # Берем только непустые записи в исходном csv.
            csv_dst.writerow(row)

    # shutil.copyfileobj(src, dst)
    dst.close()
    src.close()
    shutil.move(filename_tmp, filename)
    return 0


def is_allowed_file(filename):
    """ Проверяет входит ли разрешение исходного файла в список разрешенных

    Args:
        filename (src): Имя файла

    Returns:
        bool: True - если разрешен, False - если запрещен
    """
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in app.config['ALLOWED_EXTENSIONS']
