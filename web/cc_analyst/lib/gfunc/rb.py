# def innerHTML(src):
#     try:
#         return ''.join([str(x) for x in src])
#     except TypeError:
#         return ''

def innerHTML(element):
    return element.decode_contents(formatter="html")
