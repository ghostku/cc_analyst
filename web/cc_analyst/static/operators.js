function show_operators(operators){
	for (i in operators){
		var row = $('<tr></tr>');
		operator = operators[i];
		// nextOperator = operators[parseInt(i) + 1];
		// prevOperator = operators[parseInt(i) - 1];
		var plusCol = $('<td></td>').attr('data-editable','false');
		var nameCol = '';
		var fullNameCol = $('<td>').text(operator.full_name).attr('col', 'full_name')
		var idCol = $('<td>').text(operator.id_).attr('col', 'id_');
		var areNameCol = $('<td>').text(operator.area_name).attr({col: 'area_name', 'data-editable': false})
		if (typeof nextOperator != 'undefined') {
			if (operator.login == nextOperator.login) {
				plusCol.append($('<i class="glyphicon glyphicon-plus"></i>'))
				row.attr({
					'class': 'clickable',
					'data-toggle': 'collapse',
					'id': operator.login,
					'data-target': '.' + operator.login});
			}
		}
		row.append(plusCol);
		if ((typeof prevOperator != 'undefined') &&
				(operator.login == prevOperator.login)) {
			row.addClass('collapse').addClass(prevOperator.login);
			fromDate = new Date(0)
			fromDate.setUTCSeconds(operator.from);
			fromDate = $.format.date(fromDate, 'yyyy-MM-dd');
			nameCol = $('<td>').text(fromDate).attr('col', 'from')

		}
		else {
			nameCol = $('<td>').text(operators[i].login)
				.attr('col', 'login').attr('data-editable','false')
		}
		row.append(nameCol, fullNameCol, idCol, areNameCol);
		if (operator.id_ == 0) {row.addClass('danger')}
		$('#operators').append(row
			.attr('operator_id', operator.id)
			.attr('modify', operator.modify));
	}
	$('#operators').editableTableWidget();
	$('#operators td').on('change', function(evt, newValue){
		$(this).closest('tr').attr('modify', 'update');
	});
}

function save_operators(){
	var result = []
	$('#operators > tbody > tr').each(function() {
		if ($(this).attr('modify') == 'update'){
			var record = {
				'id': $(this).attr('operator_id'),
				'modify': $(this).attr('modify')
			}
			$(this).find('td').each(function() {
				if ($(this).attr('col') == 'from') {
					date = new Date($(this).text()) / 1000;
					record[$(this).attr('col')] = date;
				}
				else {
					record[$(this).attr('col')] = $(this).text();
				}
			})
			result.push(record);
		}
	})
	console.log(result);
	$.ajax({
		type: 'POST',
		contentType: 'application/json; charset=utf-8',
		url: $SCRIPT_ROOT + '_operators',
		dataType : 'json',
		data: JSON.stringify(result),
		success: function(data){
			$('#operators tbody').empty();
			show_operators(data);
		},
		error: function() {
			alert('Some sheet happens.');
		}
	});
}
