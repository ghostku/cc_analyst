#!/usr/bin/python3
import sys
import os
import logging

logging.basicConfig(stream=sys.stderr)
sys.path.insert(0, "/var/www/cc_analyst/")
os.environ['CC_ANALYST'] = 'config.ProductionConfig'
from cc_analyst import app as application
