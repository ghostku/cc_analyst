#ua-lat_py
# coding=utf-8
def isFirstLetter(src,index):
	if index==0: return 'beg'
	if src[index-1] in ' ': return 'beg'
	return 'other'
def translit(src):
	simple ={'А': 'A','а': 'a','Б': 'B','б': 'b','В': 'V','в': 'v','Г': 'H','г': 'h','Ґ': 'G','ґ': 'g','Д': 'D','д': 'd',
	'Е': 'E','е': 'e','З': 'Z','з': 'z','И': 'Y','и': 'y','І': 'I','і': 'i','К': 'K','к': 'k','Л': 'L','л': 'l',
	'М': 'M','м': 'm','Н': 'N','н': 'n','О': 'O','о': 'o','П': 'P','п': 'p','Р': 'R','р': 'r','С': 'S','с': 's',
	'Т': 'T','т': 't','У': 'U','у': 'u','Ф': 'F','ф': 'f','Ж': 'Zh','ж': 'zh','Х': 'Kh','х': 'kh','Ц': 'Ts',
	'ц': 'ts','Ч': 'Ch','ч': 'ch','Ш': 'Sh','ш': 'sh','Щ': 'Shch','щ': 'shch','ь':'','Э':'E','э':'e','ы':'y',' ':'_'}
	compl={'Ю': {'beg': 'Yu', 'other': 'Iu'},'ю': {'beg': 'yu', 'other': 'iu'},'Я': {'beg': 'Ya', 'other': 'Ia'},
	'я': {'beg': 'ya', 'other': 'ia'},'Ї': {'beg': 'Yi', 'other': 'I'},'ї': {'beg': 'yi', 'other': 'i'},
	'Й': {'beg': 'Y', 'other': 'I'},'й': {'beg': 'y', 'other': 'i'},'Є': {'beg': 'Ye', 'other': 'Ie'},
	'є': {'beg': 'ye', 'other': 'ie'}}
	res=''
	for i in range(0,len(src)):
		try:
			res+=compl[src[i]][isFirstLetter(src,i)]
		except KeyError:
			try:
				res+=simple[src[i]]
			except KeyError:
				res+=src[i]
	return res
if __name__ == "__main__":
	print(translit('ПЕЛ5050ХБм'))