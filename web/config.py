import os
from datetime import datetime


class Config(object):
    # Определяет включен ли режим отладки
    DEBUG = False

    # Включение защиты против "Cross-site Request Forgery (CSRF)"
    CSRF_ENABLED = True

    # Случайный ключ, которые будет исползоваться для подписи
    # данных, например cookies.
    SECRET_KEY = 'rt5t6ghj9m9b4v3c524RTFssd4'

    basedir = os.path.abspath(os.path.dirname(__file__))
    dbdir = os.path.join(basedir, 'data')
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(dbdir, 'cc_analyst.db')
    SQLALCHEMY_MIGRATE_REPO = os.path.join(dbdir, 'db_repository')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    INCOMING_ENCODING = 'cp1251'
    ALLOWED_EXTENSIONS = {'csv'}
    MAX_CALLS_PER_FILE = 20000

    MIN_DATE = datetime.utcfromtimestamp(0)
    MAX_DATE = datetime.utcfromtimestamp(32536799999)

    REDIS_URL = 'redis://127.0.0.1:6379/0'


class ProductionConfig(Config):
    REDIS_URL = 'redis://127.0.0.1:6379/0'


class DevelopmentConfig(Config):
    REDIS_URL = 'redis://127.0.0.1:6379/0'
